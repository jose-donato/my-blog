import React from 'react'
import { graphql } from "gatsby"
import Layout from "../components/Layout";
import SEO from "../components/seo";


import * as S from "../components/Post/styled"
import RecommendedPosts from '../components/RecommendedPosts';
import Comments from '../components/Comments';

const BlogPost = ({ data, pageContext }) => {
    const post = data.markdownRemark;
    const next = pageContext.nextPost
    const previous = pageContext.previousPost
    const commentUrl = data.site.siteMetadata.siteUrl 
    const title = post.frontmatter.title
    return (
        <Layout>
            <SEO title={post.frontmatter.title} description={post.frontmatter.description} image={post.frontmatter.image} />
            <S.PostHeader>
                <S.PostDate>{post.frontmatter.date} • {post.timeToRead} min of reading</S.PostDate>
                <S.PostTitle>{post.frontmatter.title}</S.PostTitle>
                <S.PostDescription>{post.frontmatter.description}</S.PostDescription>
            </S.PostHeader>
            <S.MainContent>
                <div dangerouslySetInnerHTML={{ __html: post.html }}></div>
            </S.MainContent>
            <RecommendedPosts next={next} previous={previous}/>
            <Comments url={commentUrl} slug={pageContext.slug} title={title}/>
        </Layout>
    )
}

export const query = graphql`
    query Post($slug: String!) {
        markdownRemark(fields: {slug: {eq: $slug}}) {
            frontmatter {
                title
                description
                date(locale: "pt-pt", formatString: "DD-MM-YYYY")
            }
            html
            timeToRead
        }
        site {
            siteMetadata {
                siteUrl
            }
        }
    }
`

export default BlogPost