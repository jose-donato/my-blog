import styled from "styled-components"
import media from "styled-media-query"

import Burger from "../Burger"

export const HamburgerWrapper = styled.div`
  display: block;
  position: fixed;
  top: 10px;
  right: 10px;
  z-index: 100000;

  
    ${media.greaterThan("large")`
        display: none;
    `}
`

export const HamburgerIcon = styled(Burger)`

`