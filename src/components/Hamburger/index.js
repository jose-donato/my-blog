import React from "react"
import * as S from "./styled"


const Hamburger = (props) => {
    const color = typeof window !== 'undefined' && window.getComputedStyle(document.body).getPropertyValue("--texts")

    return (
        <S.HamburgerWrapper>
            <S.HamburgerIcon direction="right" rounded
                color={color}
                toggle={props.toggle}
                toggled={props.toggled}
            />
        </S.HamburgerWrapper>
    )
}

export default Hamburger
