import React, { useState, useEffect } from 'react'

import { Home } from "@styled-icons/boxicons-solid/Home";
import { SearchAlt2 as Search } from "@styled-icons/boxicons-regular/SearchAlt2"
import { UpArrowAlt as Arrow } from "@styled-icons/boxicons-regular/UpArrowAlt"
//import { Lightbulb as Light } from "@styled-icons/remix-line/Lightbulb"
import {Moon} from "@styled-icons/boxicons-regular/Moon";
import {Sun} from "@styled-icons/evaicons-solid/Sun"
import { Grid } from "@styled-icons/boxicons-solid/Grid"
import { ThList as List } from "@styled-icons/typicons/ThList"

import * as S from './styled'

const MenuBar = () => {
    const [theme, setTheme] = useState(null);
    const [display, setDisplay] = useState(null);

    const isDarkMode = theme === 'dark'
    const isListMode = display === 'list'

    useEffect(() => {
        setTheme(window.__theme);
        setDisplay(window.__display);
        window.__onThemeChange = () => setTheme(window.__theme);
        window.__onDisplayChange = () => setDisplay(window.__display);
    }, []);

    return (
        <S.MenuBarWrapper>
            <S.MenuBarGroup>
                <S.MenuBarLink to="/" title="Back to Home">
                    <S.MenuBarItem><Home /></S.MenuBarItem>
                </S.MenuBarLink>
                <S.MenuBarLink to="/search" title="Search">
                    <S.MenuBarItem><Search /></S.MenuBarItem>
                </S.MenuBarLink>
            </S.MenuBarGroup>
            <S.MenuBarGroup>
                <S.MenuBarItem title="Change Theme" onClick={() => {
                    window.__setPreferredTheme(isDarkMode ? 'light' : 'dark')
                }}
                    className={theme}
                >
                    {/*<Light />*/}
                    {isDarkMode ? <Sun /> : <Moon />}
                </S.MenuBarItem>
                <S.MenuBarItem title="Change Visualization" onClick ={() => window.__setPreferredDisplay(isListMode ? 'grid' : 'list')} className="display">
                    {isListMode ? <Grid /> : <List />}
                </S.MenuBarItem>
                <S.MenuBarItem title="Back to top" onClick={() => { window.scrollTo({
  top: 0,
  behavior: 'smooth',
})}}>
                    <Arrow />
                </S.MenuBarItem>
            </S.MenuBarGroup>
        </S.MenuBarWrapper>
    )
}

export default MenuBar