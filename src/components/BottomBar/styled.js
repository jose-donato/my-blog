import styled from "styled-components"
import {Link} from "gatsby"
export const BottomBarWrapper = styled.nav`
    display: flex;
    flex-direction: column;
    padding-left: 1.5rem;
    padding-top: 1.5rem;
    background: var(--background);
    position: fixed;
    width: 100vw;
    bottom: 0;
    height: 40vh;
`

export const BottomBarLinksList = styled.ul`
    font-size: 1.2rem;
    font-weight: 300;
`

export const BottomBarLinksItem = styled.li`
    padding: 1.2rem 0;

    .active {
        color: var(--highlight);
    }
`

export const BottomBarLinksLink = styled(Link)`
    color: var(--texts);
    text-decoration: none;
    transition: color 0.5s;

    &:hover {
        color: var(--highlight);
    }
`

export const BottomBarLinksLinkCancel = styled.a`
    color: red;
    text-decoration: none;
    transition: color 0.5s;
    cursor: pointer;

    &:hover {
        color: #7f0000;
    }
`