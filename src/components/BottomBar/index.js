import React, { useState } from 'react'
import Hamburger from "../Hamburger"

import * as S from "./styled"
import links from "../MenuLinks/content"


const BottomBar = (props) => {
    const [isVisible, setIsVisible] = useState(false);

    const overlay = isVisible ? (
        <div
            style={{
                position: 'fixed',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                backgroundColor: 'black', opacity: 0.5
            }}
            onClick={() => {setIsVisible(false);}}
        />
    ) : null;

    const renderSheet = () => (
                <S.BottomBarWrapper>
                    <S.BottomBarLinksList>
                        {links.map(link => (
                            <S.BottomBarLinksItem key={link.id}>
                                <S.BottomBarLinksLink to={link.url} activeClassName="active">{link.label}</S.BottomBarLinksLink>
                            </S.BottomBarLinksItem>
                        ))}
                        <S.BottomBarLinksItem>
                            <S.BottomBarLinksLinkCancel onClick={() => {setIsVisible(false);}}>Cancel</S.BottomBarLinksLinkCancel>
                        </S.BottomBarLinksItem>
                    </S.BottomBarLinksList>
                </S.BottomBarWrapper>
    );



    return (
        <>
            <Hamburger toggle={setIsVisible} toggled={isVisible} />
            {props.children}
            {overlay}
            {isVisible ? renderSheet() : null}
        </>
    );



}

export default BottomBar