const links = [
    {
        id: "1",
        label: 'Home',
        url: "/"
    },
    {
        id: "2",
        label: 'Projects',
        url: "/projects"
    },
    {
        id: "3",
        label: 'Good Tutorials',
        url: "/good-tutorials"
    },
    {
        id: "4",
        label: "About Me",
        url: "/about"
    }
]


export default links;