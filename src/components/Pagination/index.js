import React from 'react'
import PropTypes from 'prop-types'
import {Link} from 'gatsby'
import * as S from "./styled"


const Pagination = ({ isFirst, isLast, currentPage, numPages, prevPage, nextPage }) => {

        return (
            <S.PaginationWrapper>
                {!isFirst && <Link to={prevPage}>← previous page</Link>}
                {currentPage} of {numPages}
                {!isLast && <Link to={nextPage}>next page →</Link>}     
            </S.PaginationWrapper>
        );
}


Pagination.propTypes = {
    isFirst: PropTypes.bool.isRequired,
    isLast: PropTypes.bool.isRequired,
    currentPage: PropTypes.number.isRequired,
    numPage: PropTypes.number.isRequired,
    prevPage: PropTypes.string,
    nextPage: PropTypes.string
}

export default Pagination