import React from 'react'
import PropTypes from 'prop-types'
import ReactDisqusComments from 'react-disqus-comments'
import * as S from './styled'

const Comments = ({url, slug, title}) => {
    return (
        <S.CommentsWrapper>
            <S.CommentsTitle>
                Comments
            </S.CommentsTitle>
            <ReactDisqusComments
                /*shortname={slug}
                identifier={slug+"dawdaw"}
                title={slug}
                url={url+slug}
                category_id={123456}*/
                shortname="josedonato"
                identifier={url+slug}
                title={title}
                url={url+slug}
            />
        </S.CommentsWrapper>
    );
}

Comments.propTypes = {
    url: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
}

export default Comments