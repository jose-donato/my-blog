import React from "react";
import propTypes from "prop-types";

import BottomBar from "../BottomBar"
import GlobalStyles from '../../styles/global'
import * as S from "./styled"
import Sidebar from "../Sidebar";
import MenuBar from "../MenuBar"



const Layout = ({ children }) => {
  return (
    <BottomBar>
    <S.LayoutWrapper>
      <GlobalStyles />

      <Sidebar />
      <S.LayoutMain>{children}</S.LayoutMain>
      <MenuBar />
    </S.LayoutWrapper>
   </BottomBar>
  );
};

Layout.propTypes = {
  children: propTypes.node.isRequired,
};

export default Layout;
