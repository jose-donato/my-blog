import { Github } from "@styled-icons/boxicons-logos/Github";
import { Twitter } from "@styled-icons/boxicons-logos/Twitter";
import { Mail } from "@styled-icons/entypo/Mail";
import { DevTo } from "@styled-icons/boxicons-logos/DevTo";

const Icons = {
    Github, Twitter, Mail, DevTo
}

export default Icons;
