const links = [
    {
        id: '1',
        label: 'Github',
        url: "https://github.com/jose-donato"
    },
    {
        id: '2',
        label: 'Twitter',
        url: "https://twitter.com/whynot1__"
    },
    {
        id: '3',
        label: 'Mail',
        url: "mailto:zmcdonato@gmail.com"
    }, 
    {
        id: '4',
        label: 'DevTo',
        url: "https://dev.to/josedonato"
    }
]

export default links;