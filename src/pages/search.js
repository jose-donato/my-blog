import React from "react";
import Layout from "../components/Layout";
import SEO from "../components/seo";
import SearchComponent from "../components/Search"

const Search = () => {
  return (
    <Layout>
      <SEO title="Search" />
      <SearchComponent />
    </Layout>
  );
};

export default Search;
